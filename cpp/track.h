#ifndef TRACK_H
#define TRACK_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>

enum trackPieceTypes { STRAIGHT = 0, BEND = 1 };

struct trackPiece 
{
  trackPieceTypes type;
  double length;
  bool  isSwitch;
  double radius;
  double angle;
  double enterVelocity;
  bool   locked;
};

class Track 
{
public:
Track();
~Track();

void addPieces( const jsoncons::json& data );
void addLanes( const jsoncons::json& data );
double getSpeed( const jsoncons::json& data );
double getSpeedLimit( const jsoncons::json& data, double& distance );
double getAcceleration();
bool isBend( const jsoncons::json& data );
void lowerSpeedLimit( const jsoncons::json& data );
void raiseSpeedLimit( const jsoncons::json& data );

private:

bool isStraight( const jsoncons::json& data, double& length );
bool isSwitch( const jsoncons::json& data, bool& isSwitch );

std::vector<trackPiece> mTrack;
int                     mCarOldIndex;
double                 mCarOldPos;
double                 mCarOldSpeed;
double                 mSpeed;
double                 mAcceleration;
double                 mBaseRadius;
double                 mBaseVelocity;

};

#endif
