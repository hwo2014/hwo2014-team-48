#include "track.h"

Track::Track()
{
  mCarOldPos = 0.0;
  mSpeed = 0.0;
  mAcceleration = 0.0;
  mBaseRadius = 0.0;
  mBaseVelocity = 0.0;
}

Track::~Track()
{
}

void Track::addPieces( const jsoncons::json& data )
{
  for (size_t i = 0; i < data.size(); ++i)
    {
        std::cout << "New piece ";
        trackPiece tmpPiece;
        tmpPiece.locked = false;
        try
        {   
            const jsoncons::json& piece = data[i];
            if ( isStraight( piece, tmpPiece.length ) )
            {
              std::cout << "STRAIGHT " << tmpPiece.length << " ";
              tmpPiece.type = trackPieceTypes::STRAIGHT;
              tmpPiece.enterVelocity = 100.0;
            }
            else
            {
              tmpPiece.type = trackPieceTypes::BEND;
              tmpPiece.radius = piece["radius"].as<double>();
              tmpPiece.angle = piece["angle"].as<double>();
              tmpPiece.length = 2.0*3.14*tmpPiece.radius * ( tmpPiece.angle / 360.0 );
              if ( tmpPiece.length < 0.0 )
              {
                tmpPiece.length = tmpPiece.length * -1.0;
              }
              tmpPiece.enterVelocity = 20 * ( ( tmpPiece.radius ) / 100.0 );
              if ( tmpPiece.enterVelocity > 30.0 )
              {
                tmpPiece.enterVelocity = 30.0;
              }
              std::cout << "BEND " << tmpPiece.radius << " " << tmpPiece.angle << " " << tmpPiece.enterVelocity << " ";
              if ( mBaseRadius == 0.0 )
              {
                mBaseRadius = tmpPiece.radius;
                mBaseVelocity = 20.0;
                std::cout << "BASERADIUS ";
              }
              
            }
            isSwitch( piece, tmpPiece.isSwitch );
            std::cout << tmpPiece.isSwitch << std::endl;
            mTrack.push_back(tmpPiece);  
        }
        catch (const std::exception& e)
        {
            std::cerr << e.what() << std::endl;
        }
    }
}

double Track::getSpeedLimit( const jsoncons::json& data, double& distance )
{
  double speedLimit = -1.0;
  try
  {
    auto& position = data["piecePosition"];
    int index = position["pieceIndex"].as<int>();
    double inDistance = position["inPieceDistance"].as<double>();
    
    if ( mTrack.at(index).type == trackPieceTypes::STRAIGHT )
    {
      distance += mTrack.at(index).length - inDistance;
    }
    index++;
    
    while ( true )
    {
      if ( index >= mTrack.size() )
      {
        index = 0;
      }

      if ( mTrack.at(index).type == trackPieceTypes::BEND )
      {
        speedLimit = mTrack.at(index).enterVelocity;
        index++;
        if ( index >= mTrack.size() )
        {
          index = 0;
        }
        if ( mTrack.at(index).enterVelocity < speedLimit )
        {
          speedLimit = mTrack.at(index).enterVelocity;
        }

        break;
      }
      else
      {
        distance += mTrack.at(index).length;
      }
      index++;
    }
    
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << std::endl;
  }

  return speedLimit;
}

double Track::getAcceleration()
{
  return mAcceleration;
}

void Track::addLanes( const jsoncons::json& data )
{
}

double Track::getSpeed( const jsoncons::json& data )
{
  double speed = 0.0;
  try
  {
    auto position = data["piecePosition"];
    int index = position["pieceIndex"].as<int>();
    double distance = position["inPieceDistance"].as<double>();
    
    if ( index == mCarOldIndex )
    { 
      speed = distance - mCarOldPos;
    }  
    else
    {
      speed = (mTrack.at(mCarOldIndex).length - mCarOldPos) + distance;
    }
    std::cout << "Speed: " << speed << " Acceleration: " << (speed - mCarOldSpeed) << std::endl;
    mCarOldIndex = index;
    mCarOldPos = distance;
    mCarOldSpeed = speed;
    mAcceleration = speed - mCarOldSpeed;
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << std::endl;
  }

  return speed;
}

bool Track::isBend( const jsoncons::json& data )
{
  bool isBend = false;
  try
  {
    auto position = data["piecePosition"];
    int index = position["pieceIndex"].as<int>();
    
    if ( mTrack.at(index).type == trackPieceTypes::BEND )
    {
      isBend = true;
    }
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << std::endl;
  }

  return isBend;
}

void Track::lowerSpeedLimit( const jsoncons::json& data )
{
  std::cout << "Decrease enterVelocity" << std::endl;
  try
  {
    auto position = data["piecePosition"];
    int index = position["pieceIndex"].as<int>();
    
    mTrack.at(index).enterVelocity -= 1.0;
    
    index--;
    if ( index < 0 )
    {
      index = mTrack.size() - 1;
    }
      
    if ( mTrack.at(index).type == trackPieceTypes::BEND )
    {
      mTrack.at(index).enterVelocity -= 1.0;
      mTrack.at(index).locked = true;
    }
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << std::endl;
  }
}

void Track::raiseSpeedLimit( const jsoncons::json& data )
{
  std::cout << "Increase enterVelocity" << std::endl;
  try
  {
    auto position = data["piecePosition"];
    int index = position["pieceIndex"].as<int>();
    
    if ( !mTrack.at(index).locked )
    {
      mTrack.at(index).enterVelocity += 0.75;
    
      index--;
      if ( index < 0 )
      {
        index = mTrack.size() - 1;
      }
      
      if ( mTrack.at(index).type == BEND )
      {
       mTrack.at(index).enterVelocity += 0.75;
      }
    }
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << std::endl;
  }
}

bool Track::isStraight( const jsoncons::json& data, double& length )
{
  bool retVal = true;
  try
  {
    length = data["length"].as<double>();
  }
  catch (const std::exception& e)
  {
    retVal = false;
  }
  return retVal;
}

bool Track::isSwitch( const jsoncons::json& data, bool& isSwitch )
{
  bool retVal = true;
  try
  {
    isSwitch = data["switch"].as<bool>();
  }
  catch (const std::exception& e)
  {
    retVal = false;
  }
  return retVal;
}

