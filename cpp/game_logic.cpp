#include "game_logic.h"
#include "protocol.h"
#include "track.h"

using namespace hwo_protocol;

game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
      { "joinRace", &game_logic::on_join },
      { "gameStart", &game_logic::on_game_start },
      { "gameInit", &game_logic::on_game_init },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error },
      { "turboAvailable", &game_logic::on_turboAvailable }
    }
{
  mTrack = new Track();
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
  mThrottle = THROTTLE_MAX;
  mOldAngle = 0.0;
  mDec = -1.0;
  mMaxAngle = 10.0;
  skipCounter = SKIP;
  mTurboAvailable = false;
  mDecreased = false;
  mIncreased = false;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
  std::cout << "Game init" << std::endl;
  try
  {
    const auto& race = data["race"];
    const auto& track = race["track"];
    const auto id = track["id"].as<std::string>();
    std::cout << id << std::endl;
    auto pieces = track["pieces"];
    mTrack->addPieces( pieces );
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << std::endl;
  }
  
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
  if ( skipCounter < SKIP )
  {
    skipCounter++;
    return { make_throttle(mThrottle) };
  }
  skipCounter = 0;
  
  for (size_t i = 0; i < data.size(); ++i)
    {
        try
        {
            const auto& car = data[i];
            const auto& carID = car["id"];
            auto name = carID["name"].as<std::string>();
            
            if ( name == "TurboSaba" )
            {
              auto angle = car["angle"].as<double>();
              if ( angle < 0 )
              {
                angle = angle * -1;
              }
              
              double speed = mTrack->getSpeed( car );
              double acc = mTrack->getAcceleration();
              
              if ( !mTrack->isBend( car ) )
              {
                // driving straight
                mDecreased = false;
                mIncreased = false;
                double distance = 0.0;
                double speedLimit = mTrack->getSpeedLimit( car, distance );
                double speedDelta = speed - speedLimit;;
                
                std::cout << "Speedlimit: " << speedLimit << " distance: " << distance << std::endl;
              
                if ( speedDelta < 0.0 )
                {
                  // Not going fast enough
                  mThrottle = THROTTLE_MAX;
                  if ( ( distance > 500.0 ) && mTurboAvailable )
                  {
                    std::cout << "TURBO Speedlimit: " << speedLimit << " distance: " << distance << std::endl;
                    mTurboAvailable = false;
                    return { make_turbo() };
                  }
                }
                else
                {
                  double brakeDist = ( speed + speedDelta / 2 ) * ( speedDelta / ( mDec * -1 ) );
                  
                  std::cout << "Brake distance: " << brakeDist << std::endl;
                  
                  if ( brakeDist  > distance )
                  {
                    if  ( speedDelta > 3.0 )
                    {
                      mThrottle = 0;
                    }
                    else
                    {
                      mThrottle = 0.4;
                    }
                  }
                  else
                  {
                    mThrottle = THROTTLE_MAX;
                  }
                }
              }
              else
              {
              // Driving through bend
              // Tarkkaile kulmaa, valitse nopeus, tallenna maksimikulma, korjaa speedlimittiä
                
                // driving straight
                double distance = 0.0;
                double speedLimit = mTrack->getSpeedLimit( car, distance );
                std::cout << "Angle: " << angle << "Speedlimit: " << speedLimit << std::endl;
                
                if ( angle > 50.0 )
                {
                  if ( !mDecreased )
                  {
                    mTrack->lowerSpeedLimit( car );
                    mDecreased = true;
                  }
                  mThrottle = 0.0;
                }
                else if ( ( angle <= mOldAngle || angle < 40.0 ) && mThrottle < THROTTLE_MAX )
                {
                  mThrottle += 0.1;
                  if ( angle < mOldAngle )
                  {
                    mThrottle += 0.1;
                  }
                  if ( angle < 15.0 || angle < mOldAngle )
                  {
                    if ( mMaxAngle < 37.0  && !mIncreased )
                    {
                      mIncreased = true;
                      mTrack->raiseSpeedLimit( car );
                      mMaxAngle = 10.0;
                    }
                    mThrottle += 0.4;
                  }
                  if ( mThrottle > 1.0 )
                  {
                    mThrottle = 1.0;
                  }
                }
                
                if ( speed > speedLimit || angle > 35 || ( angle - mOldAngle ) > 7.0 )
                {
                  mThrottle -= 0.25;
                  if ( speed > speedLimit )
                  {
                    mThrottle -= 0.25;
                    if ( speed - speedLimit > 3 )
                    {
                      mThrottle = 0.0;
                    }
                  }
                  if ( ( angle - mOldAngle ) > 10 )
                  {
                    mThrottle -= 0.3;
                  }
                  if ( mThrottle < THROTTLE_MIN )
                  {
                    mThrottle = THROTTLE_MIN;
                  }
                }
                
                mOldAngle = angle;
                if ( angle > mMaxAngle )
                {
                  mMaxAngle = angle;
                }
              }
            }
            else
            {
            // NOT my car
            }
        }
        catch (const std::exception& e)
        {
            std::cerr << e.what() << std::endl;
        }
    }
  std::cout << "Throttle: " << mThrottle << std::endl;
  return { make_throttle(mThrottle) };
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  std::cout << "Someone crashed" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_turboAvailable(const jsoncons::json& data)
{
  std::cout << "Turbo Available!" << std::endl;
  mTurboAvailable = true;
  return { make_ping() };
}
